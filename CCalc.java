/******************************************************************************
 * Console calculator
 * 
 * Author:  Kirill V. Filimonov
 * Written: 04/04/13
 * 
 * Compilation: javac CCalc.java
 * Execution:	java CCalc
 * 
 * Evaluates an expression from console input
 * Supported operand: 		floats, ints.
 * Supported operations:	+, -, /, *, ().
 * 
 * %java CCalc
 * 2+2*2
 * 6
 *****************************************************************************/

import java.util.Stack;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

public class CCalc {
	private Stack<Double> vals;	// values
	private Stack<String> ops;  // operations 
	private TreeMap<String, Integer> prior; // priority of operations
	private static TreeMap<String, Integer> numVals; // types of operations
	
	// regex pattern for matching values from input
	private String valuePattern = "(^-\\d+\\.\\d+)|(^-\\d+)|(\\(-\\d+\\))|(\\(-\\d+\\.\\d+\\))|(\\d+\\.\\d+)|(\\d+)";
	// regex pattern for matching operations from input
	private String operationPattern = "([\\+\\-\\*/\\(\\)])";
	// regex pattern for mathing negative values from values
	private String negativePattern = "((-*\\d+\\.\\d+)|(-*\\d+))";
	
	public CCalc() {
		vals = new Stack<Double>();
		ops = new Stack<String>();
		prior = new TreeMap<String, Integer>();
		numVals = new TreeMap<String, Integer>();
		
		// filling in priority of operations
		prior.put("(", 0);
		prior.put(")", 0);
		prior.put("+", 1);
		prior.put("-", 1);
		prior.put("*", 2);
		prior.put("/", 2);
		
		// filling in types of operations
		numVals.put("(", 1);
		numVals.put(")", 1);
		numVals.put("+", 2);
		numVals.put("-", 2);
		numVals.put("*", 2);
		numVals.put("/", 2);
	}
	
	/* evaluating operation
	 * input: String - sign of operation
	 * 		  Stack - values
	 * 
	 * output: Double - result of operation 
	 */
	private static double eval(String op, Stack<Double> val) {
		if (numVals.get(op) == 2) {
			double val1 = val.pop();
			double val2 = val.pop();
			if (op.equals("/") && val2 == 0) 
				throw new ArithmeticException("Divizion by zero");
			else if (op.equals("/")) return val1 / val2;
			else if (op.equals("*")) return val1 * val2;
			else if (op.equals("+")) return val1 + val2;
			else if (op.equals("-")) return val1 - val2;
			else throw new RuntimeException("Invalid Operator");
		}
		else
			throw new RuntimeException("Invalid number of arguments");
	}
		
	/* Test validity of operation
	 * input: sign of operation
	 * 
	 * Checks if there is enough values for performing operation
	 * if not raise an exception
	 */
	private void testValidity(String op) {
		if (numVals.get(op) > vals.size())
			throw new IllegalStateException("Invalid operation");
	}
	
	/* Reading input string and performing calculation
	 * input: String with expression
	 * 
	 * Parsing the string using regular expressions, filling 
	 * in data structures for values and operations. Performing 
	 * calculation using Dijkstra's two-stack algorithm. Resulting 
	 * value pushes onto value stack.
	 */
	private void stringParse(String in) {
		Pattern p = Pattern.compile("(" + valuePattern + "|" 	// creating pattern	
									+ operationPattern + ")");	// for input
		Matcher m = p.matcher(in);	// match characters using pattern
		while (m.find()) {
			String s = m.group();	
			if (!prior.containsKey(s)) {	// check if not an operation 
				Pattern p1 = Pattern.compile(negativePattern); // match negative numbers
				Matcher m1 = p1.matcher(s);
				String num = s;
				while (m1.find())
					num = m1.group();
				vals.push(Double.parseDouble(num)); // add values to stack
				continue;
			}
			
			while (true) { // perform calculation until lower priority operator in stack
				if (ops.isEmpty() || s.equals("(")
						|| prior.get(s) > prior.get(ops.peek())) {
					ops.push(s);
					break;
				}
			
				String op = ops.pop();
				
				if (op.equals("(")) break; 
				else 
					evaluate(op);
			}
		}
		// perfom all operations from stack, when input string is empty
		while (!ops.isEmpty()) {
			String op = ops.pop();
			evaluate(op);
		}
	}
	
	/* prepare values for calculation and invoke eval method
	 * input: String - sign of operation
	 * 
	 * Push result of operation onto values stack
	 */
	private void evaluate(String op) {
		testValidity(op);
		Stack<Double> val = new Stack<Double>();
		val.push(vals.pop());
		val.push(vals.pop());
		vals.push(eval(op, val));
	}
	
	/* Public method for calculating expression from input
	 * input: string with expression
	 * output: double - result of calculation
	 * 
	 * Performing calculation by invoking private method
	 * for calculation algorithm.
	 */
	public double calculate(String in) {
		stringParse(in);	// parsing and calculating
		return vals.pop();	// get resulting value from value stack
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String in = scan.nextLine();
		scan.close();
		CCalc calc = new CCalc();
		try {
			double res = calc.calculate(in);
			System.out.println(res);
		}
		catch (Exception e) {
			System.out.println(e.toString());
			System.exit(-1);
		}		
	}

}
