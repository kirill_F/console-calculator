import static org.junit.Assert.*;

import org.junit.Test;


public class testCalc {

	@Test
	public void testNewInstance() {
		CCalc calc = new CCalc();
		assertEquals(calc.getClass(), CCalc.class);
	}
	
	@Test
	public void testAddition() {
		CCalc calc = new CCalc();
		String in = "1+1";
		assertTrue(calc.calculate(in) == 2.0);
	}
	
	@Test
	public void testSubtraction() {
		CCalc calc = new CCalc();
		String in = "2-1";
		assertTrue(calc.calculate(in) == 1.0);
	}
	
	@Test
	public void testMultiplication() {
		CCalc calc = new CCalc();
		String in = "2*3";
		assertTrue(calc.calculate(in) == 6.0);
	}
	
	@Test
	public void testDivision() {
		CCalc calc = new CCalc();
		String in = "10/5";
		assertTrue(calc.calculate(in) == 2.0);
	}
	
	@Test
	public void testMultAddition() {
		CCalc calc = new CCalc();
		String in = "2+3+4";
		assertTrue(calc.calculate(in) == 9.0);
	}
	
	@Test
	public void testMultSubtraction() {
		CCalc calc = new CCalc();
		String in = "5-5-5";
		assertTrue(calc.calculate(in) == -5.0);
	}
	
	@Test
	public void testMultMultiplication() {
		CCalc calc = new CCalc();
		String in = "2*3*4";
		assertTrue(calc.calculate(in) == 24.0);
	}
	
	@Test
	public void testMultDivision() {
		CCalc calc = new CCalc();
		String in = "2/4/8";
		assertTrue(calc.calculate(in) == 0.0625);
	}
	
	@Test
	public void testMixed() {
		CCalc calc = new CCalc();
		String in = "1+2*3-4/2+10";
		assertTrue(calc.calculate(in) == 15.0);
	}
	
	@Test
	public void testParenMixed1() {
		CCalc calc = new CCalc();
		String in = "(1+2)*(2+3)";
		assertTrue(calc.calculate(in) == 15.0);
	}

	@Test(expected=ArithmeticException.class)
	public void testDivisionByZero() {
		CCalc calc = new CCalc();
		String in = "1+2/0";
		calc.calculate(in);
	}
	
	//@Test(expected=RuntimeException.class)
	public void testOneArg() {
		CCalc calc = new CCalc();
		String in = "1++";
		calc.calculate(in);
	}
	
	//@Test(expected=RuntimeException.class)
	public void testNoArg() {
		CCalc calc = new CCalc();
		String in = "";
		calc.calculate(in);
	}
	
	@Test(expected=RuntimeException.class)
	public void testWrongNumArgs() {
		CCalc calc = new CCalc();
		String in = "1+";
		calc.calculate(in);
	}
	
	@Test(expected=RuntimeException.class)
	public void testWrongNumArgs2() {
		CCalc calc = new CCalc();
		String in = "1+2*5-(3+)";
		calc.calculate(in);
	}
	
	@Test(expected=RuntimeException.class)
	public void testinvalidOperation() {
		CCalc calc = new CCalc();
		String in = "1+2(";
		calc.calculate(in);
	}
	
	@Test
	public void testNegativeInput() {
		CCalc calc = new CCalc();
		String in = "-1+2";
		assertTrue(calc.calculate(in) == 1.0);
		in = "2+(-3)*6";
		assertTrue(calc.calculate(in) == -16.0);
	}
	
	@Test
	public void testFloatingPoint() {
		CCalc calc = new CCalc();
		String in = "0.5+1";
		assertTrue(calc.calculate(in) == 1.5);
		in = "0.5*(-0.5)";
		assertTrue(calc.calculate(in) == -0.25);
		in = "-0.5*0.5";
		assertTrue(calc.calculate(in) == -0.25);
	}
	
	@Test
	public void testFormatting() {
		CCalc calc = new CCalc();
		String in = "(12  	+ (-12)	)  			 * 356";
		assertTrue(calc.calculate(in) == 0.0);
	}
	
}
